<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Color
 * @package App
 */
class Color extends Model
{
    protected $fillable = [ 'name', 'hex', 'rgb', 'rgba' ];
}
