<?php

namespace App\Http\Controllers;

use App\Car;
use App\Http\Requests\StoreNewCar;
use Illuminate\Http\RedirectResponse;

/**
 * Class CarController
 * @package App\Http\Controllers
 */
class CarController extends Controller
{
    /**
     * @param StoreNewCar $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreNewCar $request): RedirectResponse
    {
        Car::create([
            'brand_id' => $request->get('brand_id'),
            'model' => $request->get('model'),
            'seats' => $request->get('seats'),
            'color_id' => $request->get('color_id'),
            'year' => $request->get('year'),
        ]);

        return redirect()->route('home');
    }
}
