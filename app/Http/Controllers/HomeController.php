<?php

namespace App\Http\Controllers;

use App\{
    Brand, Color, Country
};
use App\Http\Repositories\CarRepository;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /** @var CarRepository */
    private $car;

    /**
     * HomeController constructor.
     * @param CarRepository $carRepository
     */
    public function __construct(CarRepository $carRepository)
    {
        $this->car = $carRepository;
    }

    /**
     * @param Request $request
     * @return View
     */
    public function index(Request $request): View
    {
        return view('main', [
            'cars'      => $this->car->getAll($request->get('filter')),
            'brands'    => Brand::all(),
            'countries' => Country::all(),
            'colors'    => Color::all(),
            'filter'    => $request->get('filter'),
        ]);
    }
}
