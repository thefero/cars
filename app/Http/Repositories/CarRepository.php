<?php
namespace App\Http\Repositories;

use App\Car;

/**
 * Class CarRepository
 * @package App\Http\Repositories
 */
class CarRepository
{

    /**
     * @param array|null $filters
     * @return mixed
     */
    public function getAll(? array $filters = [])
    {
        $cars = Car::join('brands', 'cars.brand_id', '=', 'brands.id')
            ->orderBy('brands.name')
            ->with([ 'brand.country', 'color' ]);

        if (isset($filters['seats']) &&  $filters['seats']) {
            $cars->where('seats', '=', $request->get('seats'));
        }

        if (isset($filters['brand']) &&  $filters['brand']) {
            $cars->where('brand_id', '=', $filters['brand']);
        }

        if (isset($filters['country']) &&  $filters['country']) {
            $cars->where('brands.country_id', '=', $filters['country']);
        }

        if (isset($filters['color']) &&  $filters['color']) {
            $cars->where('cars.color_id', '=', $filters['color']);
        }

        if (isset($filters['min_year']) &&  $filters['min_year']) {
            $cars->where('cars.year', '>=', $filters['min_year'] . '-01-01');
        }

        if (isset($filters['max_year']) &&  $filters['max_year']) {
            $cars->where('cars.year', '<=', $filters['max_year'] . '-01-01');
        }

        return $cars->get();
    }
}