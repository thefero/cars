<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreNewCar
 * @package App\Http\Requests
 */
class StoreNewCar extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'brand_id.required' => 'The brand field is required',
            'color_id.required' => 'The color field is required',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'brand_id' => 'required|integer',
            'model' => 'required',
            'seats' => 'required',
            'color_id' => 'required|integer',
            'year' => 'required|integer',
        ];
    }
}
