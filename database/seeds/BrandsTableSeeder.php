<?php

use Illuminate\Database\Seeder;
use Symfony\Component\Yaml\Yaml;
use App\{
    Brand, Country
};

/**
 * Class BrandsTableSeeder
 */
class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brands = Yaml::parse(file_get_contents(__DIR__ . '/brands.yaml'));

        foreach ($brands as $brand) {
            /** @var Country $country */
            $country = Country::where('name', '=', $brand['country'])->first();

            Brand::create([
                'name' => $brand['name'],
                'country_id' => $country->id,
            ]);
        }
    }
}
