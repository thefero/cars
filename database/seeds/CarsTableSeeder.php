<?php

use Illuminate\Database\Seeder;
use Symfony\Component\Yaml\Yaml;
use App\{
    Car, Brand, Color
};

/**
 * Class CarsTableSeeder
 */
class CarsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cars = Yaml::parse(file_get_contents(__DIR__ . '/cars.yaml'));

        foreach ($cars as $car) {
            /** @var Color $country */
            $color = Color::where('name', '=', $car['color'])->first();

            /** @var Brand $brand */
            $brand = Brand::where('name', '=', $car['brand'])->first();

            Car::create([
                'brand_id' => $brand->id,
                'model' => $car['model'],
                'seats' => $car['seats'],
                'color_id' => $color->id,
                'year' => $car['year'],
            ]);
        }
    }
}
