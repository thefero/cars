<?php

use Illuminate\Database\Seeder;
use Symfony\Component\Yaml\Yaml;
use App\Color;

/**
 * Class ColorsTableSeeder
 */
class ColorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $colors = Yaml::parse(file_get_contents(__DIR__ . '/colors.yaml'));

        foreach ($colors as $color) {
            Color::create([
                'name' => $color['name'],
                'hex' => $color['hex'],
                'rgb' => $color['rgb'],
                'rgba' => $color['rgba'],
            ]);
        }
    }
}
