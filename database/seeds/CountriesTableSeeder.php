<?php

use Illuminate\Database\Seeder;
use Symfony\Component\Yaml\Yaml;
use App\Country;

/**
 * Class CountriesTableSeeder
 */
class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = Yaml::parse(file_get_contents(__DIR__ . '/countries.yaml'));

        foreach ($countries as $country) {
            Country::create([
                'name' => $country['name']
            ]);
        }
    }
}
