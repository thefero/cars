<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(ColorsTableSeeder::class);
         $this->call(CountriesTableSeeder::class);
         $this->call(BrandsTableSeeder::class);
         $this->call(CarsTableSeeder::class);
    }
}
