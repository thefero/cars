require('./bootstrap');

import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import Datepicker from 'vuejs-datepicker';

Vue.use(BootstrapVue);

new Vue({
    el: '#app'
    , data: {
        brands
        , countries
        , colors
        , filter
    }
    , components: {
        Datepicker
    }
    , methods: {
        resetFilterForm() {
            this.$el.querySelectorAll('#filter-cars .form-control').forEach(field => {
                switch (field.tagName.toLowerCase()) {
                    case 'select':
                        field.selectedIndex = 0;
                        break;
                    default:
                        field.value = '';
                }

                document.getElementById('filter-cars').submit()
            });
        }
    }
});
