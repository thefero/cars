<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>CarsRUs</title>
        <meta name="description" content="">

        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        @yield('styles')
    </head>
    <body>
        <div id="app">
            <nav class="navbar navbar-dark bg-dark mb-5">
                <a class="navbar-brand" href="/">Cars R Us</a>
            </nav>

            <div class="container">
                @yield('content')
            </div>
        </div>

        @yield('initialScripts')
        <script src="{{ asset('js/app.js') }}"></script>
        @yield('scripts')
    </body>
</html>