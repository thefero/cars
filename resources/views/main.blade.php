@extends('layouts.app')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <h4>There was an error saving the form</h4>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add new car</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('store_car') }}" method="get" id="new-car-form">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="brand">Brand</label>
                                <select name="brand_id" id="brand" class="form-control">
                                    <option selected disabled>Choose...</option>
                                    @foreach ($brands as $brand)
                                        <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="model">Model</label>
                                <input type="text" name="model" class="form-control" id="model" placeholder="Carrera GT3">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="seats">Number of seats</label>
                                <input type="email" name="seats" class="form-control" id="seats" placeholder="4">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="color">Color</label>
                                <select id="color" name="color_id" class="form-control">
                                    <option selected disabled>Choose...</option>
                                    @foreach ($colors as $color)
                                        <option value="{{ $color->id }}">{{ $color->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="year">Year</label>
                                <input type="number" name="year" class="form-control" id="year" placeholder="2011">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button
                            type="button"
                            class="btn btn-primary"
                            onclick="event.preventDefault();
                                document.getElementById('new-car-form').submit();"
                    >Save</button>
                </div>
            </div>
        </div>
    </div>

    <b-card bg-variant="light"
            header="Filtering">
        <b-form action="{{ route('home') }}" method="get" id="filter-cars">
            <div class="row">
                <div class="form-group col-md-2">
                    <label class="sr-only" for="filter-brand">Brand</label>
                    <b-form-select class="form-control-sm"
                                   :value="null"
                                   :options="brands"
                                   id="filter-brand"
                                   name="filter[brand]"
                                   v-model="filter['brand']">
                        <option slot="first" :value="null">Filter by brand</option>
                    </b-form-select>
                </div>
                <div class="form-group col-md-2">
                    <label class="sr-only" for="filter-country">Country</label>
                    <b-form-select class="form-control-sm"
                                   :value="null"
                                   :options="countries"
                                   id="filter-country"
                                   name="filter[country]"
                                   v-model="filter['country']">
                        <option slot="first" :value="null">Filter by country</option>
                    </b-form-select>
                </div>
                <div class="form-group col-md-2">
                    <label class="sr-only" for="filter-seats">Seats</label>
                    <b-input id="filter-seats" class="form-control-sm" name="filter[seats]" placeholder="Number of seats" v-model="filter['seats']" />
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-2">
                    <label class="sr-only" for="filter-color">Color</label>
                    <b-form-select class="form-control-sm"
                                   :value="null"
                                   :options="colors"
                                   id="filter-color"
                                   name="filter[color]"
                                   v-model="filter['color']">
                        <option slot="first" :value="null">Filter by color</option>
                    </b-form-select>
                </div>
                <div class="form-group col-md-2">
                    <label class="sr-only" for="filter-min-year">Minimum year</label>
                    <datepicker input-class="form-control form-control-sm" :minimum-view="'year'" :maximum-view="'year'" :initial-view="'year'" :name="'filter[min_year]'" :placeholder="'Newer than'" format="yyyy" :value="filter.min_year"></datepicker>
                </div>
                <div class="form-group col-md-2">
                    <label class="sr-only" for="filter-max-year">Maximum year</label>
                    <datepicker input-class="form-control form-control-sm" :minimum-view="'year'" :maximum-view="'year'" :initial-view="'year'" :name="'filter[max_year]'" :placeholder="'Older than'" format="yyyy" :value="filter.max_year"></datepicker>
                </div>
                <div class="form-group col-md-2">
                    <button
                            type="button"
                            class="btn btn-primary btn-sm"
                            onclick="event.preventDefault();
                                document.getElementById('filter-cars').submit();"
                    >Filter</button>

                    <button
                            type="button"
                            class="btn btn-link btn-sm"
                            @click="resetFilterForm"
                    >Reset</button>
                </div>
            </div>
        </b-form>
    </b-card>

    <div class="py-3">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
            Add new car
        </button>
    </div>

    <div class="py-3">
        <table class="table table-striped table-bordered">
            <thead class="thead-dark">
                <tr>
                    <th>Brand</th>
                    <th>Model</th>
                    <th>Seats</th>
                    <th>Color</th>
                    <th>Year</th>
                    <th>Brand Country</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($cars as $car)
                    <tr>
                        <td>{{ $car->brand->name }}</td>
                        <td>{{ $car->model }}</td>
                        <td>{{ $car->seats }}</td>
                        <td>{{ $car->color->name }}</td>
                        <td>{{ $car->year }}</td>
                        <td>{{ $car->brand->country->name }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection

@section('initialScripts')
    <script>
        let brands = {}
            , countries = {}
            , colors = {}
            , filter = {
                brand: null
                , color: null
                , country: null
                , seats: null
                , min_year: null
                , max_year: null
            };

        @foreach ($brands as $brand)
            brands[{{ $brand->id }}] = ' {{ $brand->name }}';
        @endforeach

        @foreach ($countries as $country)
            countries[{{ $country->id }}] = ' {{ $country->name }}';
        @endforeach

        @foreach ($colors as $color)
            colors[{{ $color->id }}] = ' {{ $color->name }}';
        @endforeach

        @if ($filter['brand'])
            filter.brand = {{ $filter['brand'] }};
        @endif

        @if ($filter['color'])
            filter.color = {{ $filter['color'] }};
        @endif

        @if ($filter['country'])
            filter.country = {{ $filter['country'] }};
        @endif

        @if ($filter['seats'])
            filter.seats = {{ $filter['seats'] }};
        @endif

        @if ($filter['min_year'])
            filter.min_year = {{ $filter['min_year'] }} + '-01-01';
        @endif

        @if ($filter['max_year'])
            filter.max_year = {{ $filter['max_year'] }} + '-01-01';
        @endif
    </script>
@endsection