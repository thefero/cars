<?php

Route::get('/', 'HomeController@index')->name('home');
Route::get('/cars/new', 'CarController@store')->name('store_car');